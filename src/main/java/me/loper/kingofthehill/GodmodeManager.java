package me.loper.kingofthehill;

import com.earth2me.essentials.Essentials;
import org.bukkit.entity.Player;

public class GodmodeManager {
    private Essentials essentials;

    public GodmodeManager(KingOfTheHill plugin) {
        if (plugin.getServer().getPluginManager().isPluginEnabled("Essentials")) {
            this.essentials = (Essentials) plugin.getServer().getPluginManager()
                    .getPlugin("Essentials");
        }
    }

    public boolean isGodModeEnabled(Player playerx) {
        if (null != this.essentials) {
            return this.essentials.getUser(playerx).isGodModeEnabled();
        }

        return false;
    }
}
