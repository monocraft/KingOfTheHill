package me.loper.kingofthehill;

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class KingHandler extends BukkitRunnable {

    private final GodmodeManager godModeManager;
    private final KingOfTheHill plugin;

    private String currentKing;
    private int doubleTime;
    private int time;

    public KingHandler(KingOfTheHill plugin) {
        this.plugin = plugin;
        this.godModeManager = new GodmodeManager(plugin);
    }

    public void run() {
        List<Player> players = this.plugin.getLocation().getWorld().getPlayers();

        if (0 == players.size()) {
            return;
        }

        List<Player> topPlayers = this.getTopPlayers(players);

        if (topPlayers.size() == 0) {
            this.currentKing = null;
            this.resetTime();
            return;
        }

        int minOnline = this.plugin.getConfig().getInt("min_online");
        if (Bukkit.getOnlinePlayers().size() < minOnline) {
            String smallOnlineMessage = this.plugin.getConfig().getString("smallOnlineMessage")
                    .replace("%online", String.valueOf(minOnline));
            topPlayers.forEach(p -> p.sendMessage(this.parseColor(smallOnlineMessage)));

            return;
        }

        this.handleTopPlayers(topPlayers);
    }

    private List<Player> getTopPlayers(List<Player> players) {
        ArrayList<Player> topPlayers = Lists.newArrayList();

        for (Player p : players) {
            Location playerLocation = p.getLocation();

            if (playerLocation.getBlockX() != this.plugin.getLocation().getBlockX()) {
                continue;
            }

            if (playerLocation.getBlockY() < this.plugin.getLocation().getBlockY() ||
                playerLocation.getBlockY() > this.plugin.getLocation().getBlockY() + 1
            ) {
                continue;
            }

            if (playerLocation.getBlockZ() != this.plugin.getLocation().getBlockZ()) {
                continue;
            }

            if (p.getAllowFlight() || this.godModeManager.isGodModeEnabled(p) || !p.getGameMode().equals(GameMode.SURVIVAL)) {
                p.sendMessage(this.parseColor(this.plugin.getConfig().getString("fly_or_creative_message")));
                continue;
            }

            topPlayers.add(p);
        }

        return topPlayers;
    }

    private void handleTopPlayers(List<Player> topPlayers) {
        if (topPlayers.size() == 1) {
            handleKing(topPlayers);

            return;
        }

        if (this.currentKing == null) {
            return;
        }

        Player currentKing = Bukkit.getPlayerExact(this.currentKing);
        if (topPlayers.contains(currentKing) && ++this.doubleTime < 20) {

            if (this.doubleTime % 2 == 0) {
                currentKing.sendMessage(this.parseColor(this.plugin.getConfig().getString("king_more_players_remain_message").replace("%time", String.valueOf((20 - this.doubleTime) / 2))));
            }

            String alreadyHasCurrentKing = this.plugin.getConfig()
                    .getString("already_has_current_king")
                    .replace("%name", currentKing.getName());

            if (this.doubleTime % 2 == 0) {
                topPlayers.stream().filter(p -> !currentKing.getName().equals(p.getName()))
                        .forEach(p -> p.sendMessage(this.parseColor(alreadyHasCurrentKing)));
            }

            return;
        }

        if (topPlayers.contains(currentKing)) {
            this.plugin.pushPlayer(currentKing);
            currentKing.sendMessage(this.parseColor(this.plugin.getConfig().getString("king_more_players_message")));
        }

        this.currentKing = null;
        this.resetTime();
    }

    private void handleKing(List<Player> topPlayers) {
        Player currentKing = topPlayers.get(0);

        if (currentKing.getName().equalsIgnoreCase(this.plugin.getKing())) {
            currentKing.sendMessage(this.parseColor(this.plugin.getConfig().getString("already_king_message")));
            this.plugin.getServer().getScheduler().runTask(this.plugin, () -> this.plugin.pushPlayer(currentKing));

            return;
        }

        if (!currentKing.getName().equalsIgnoreCase(this.currentKing)) {
            this.currentKing = currentKing.getName();
            this.resetTime();
            return;
        }

        int timeout = this.plugin.getConfig().getInt("timeout");

        if (this.time % 2 == 0) {
            String remainTimeMessage = this.plugin.getConfig().getString("remain_time_message");
            String remainTime = String.valueOf((timeout * 2 - this.time) / 2);

            currentKing.sendMessage(this.parseColor(remainTimeMessage.replace("%time", remainTime)));
        }

        if (++this.time >= timeout * 2) {
            this.plugin.setKing(currentKing.getName());

            this.plugin.pushPlayer(currentKing);
            this.currentKing = null;
            this.doubleTime = 0;

            this.plugin.getServer().getScheduler().runTask(this.plugin,
                () -> this.plugin.performActions(currentKing));
        }
    }

    private void resetTime() {
        this.doubleTime = 0;
        this.time = 0;
    }

    public String parseColor(String color) {
        return ChatColor.translateAlternateColorCodes('&', color);
    }
}
