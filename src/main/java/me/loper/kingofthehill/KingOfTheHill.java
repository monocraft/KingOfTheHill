package me.loper.kingofthehill;

import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.List;
import java.util.Set;

public class KingOfTheHill extends JavaPlugin {
    private Location location;
    private BukkitTask task;
    private Chat vaultChat;
    private String king;

    public void onEnable() {
        this.saveDefaultConfig();

        RegisteredServiceProvider<Chat> chatProvider = this.getServer()
                .getServicesManager().getRegistration(Chat.class);

        if (chatProvider == null) {
            this.setEnabled(false);
            this.getLogger().severe("Permission plugin was not loaded!");
            return;
        }

        this.vaultChat = chatProvider.getProvider();
        this.location = this.parseLocation(this.getConfig().getString("top_location"));

        if (this.getServer().getPluginManager().getPlugin("PlaceholderAPI") != null) {
            this.getLogger().info("Successfully registered placeholder.");
            new KingExpansion(this).register();
        }

        this.getCommand("koth").setExecutor((sender, command, s, args) -> {
            if (!(sender instanceof Player) || sender.hasPermission("koth.admin")) {
                sender.sendMessage(ChatColor.DARK_RED + "Данную команду может выполнять только игрок.");
                return true;
            }

            this.getConfig().set("top_location", this.parseLocation(((Player) sender).getLocation()));
            this.saveConfig();

            this.location = this.parseLocation(this.getConfig().getString("top_location"));
            sender.sendMessage(ChatColor.GREEN + "успешно");

            return true;
        });

        this.task = new KingHandler(this).runTaskTimerAsynchronously(this, 10L, 10L);
    }

    public void onDisable() {
        this.task.cancel();
        this.task = null;
        this.vaultChat = null;
        this.location = null;
    }

    public void performActions(Player player) {
        ConfigurationSection actions = this.getConfig().getConfigurationSection("actions");

        for (String actionName : actions.getKeys(false)) {
            String prefix = this.vaultChat.getGroupPrefix(player.getWorld(), this.vaultChat.getPlayerGroups(player)[0]);
            String path = String.format("actions.%s", actionName);
            String action = this.getConfig().getString(path, "");

            action = this.parseColor(action
                    .replace("%prefix", prefix)
                    .replace("%name", player.getName()));

            if (actionName.equalsIgnoreCase("message")) {
                player.sendMessage(this.parseColor(action));
                continue;
            }

            if (actionName.equalsIgnoreCase("broadcast")) {
                Bukkit.broadcastMessage(this.parseColor(action));
                continue;
            }

            if (actionName.equalsIgnoreCase("title")) {
                String[] titles = this.parseColor(action).split(";");
                player.sendTitle(titles[0], titles.length > 1 ? titles[1] : "");
                continue;
            }

            if (!actionName.equalsIgnoreCase("command")) {
                this.getLogger().severe(String.format("Unknown type of action - '%s'", actionName));
                continue;
            }

            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), action);
        }
    }

    void pushPlayer(Player player) {
        Location location = player.getLocation().clone();
        location.setPitch(0.0F);
        player.setVelocity(location.getDirection().multiply(1).setY(0.5D));
    }

    private String parseLocation(Location location) {
        return location.getWorld().getName() + ";" + location.getBlockX() + ";" + location.getBlockY() + ";" + location.getBlockZ();
    }

    private Location parseLocation(String arg0) {
        String[] arg1 = arg0.split(";");
        return new Location(Bukkit.getWorld(arg1[0]),
                Double.parseDouble(arg1[1]),
                Double.parseDouble(arg1[2]),
                Double.parseDouble(arg1[3]));
    }

    private String parseColor(String arg0) {
        return ChatColor.translateAlternateColorCodes('&', arg0);
    }

    public String getKing() {
        return this.king;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setKing(String name) {
        this.king = name;
    }
}
