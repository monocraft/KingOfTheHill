package me.loper.kingofthehill;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class KingExpansion extends PlaceholderExpansion {

    private final KingOfTheHill plugin;

    public KingExpansion(KingOfTheHill plugin) {
        this.plugin = plugin;
    }

    @Override
    public @NotNull String getIdentifier() {
        return "koth";
    }

    @Override
    public @Nullable String onRequest(OfflinePlayer player, @NotNull String params) {
        return this.getKing(params);
    }

    @Override
    public @Nullable String onPlaceholderRequest(Player player, @NotNull String params) {
        return this.getKing(params);
    }

    @Nullable
    private String getKing(@NotNull String params) {
        if (!params.equals("king")) {
            return null;
        }

        String emptyPlaceholder = this.plugin.getConfig().getString("placeholder_empty");

        return this.plugin.getKing() == null
                ? ChatColor.translateAlternateColorCodes('&', emptyPlaceholder)
                : this.plugin.getKing();
    }

    @Override
    public @NotNull String getAuthor() {
        return "loper";
    }

    @Override
    public @NotNull String getVersion() {
        return "1.0.0";
    }
}
